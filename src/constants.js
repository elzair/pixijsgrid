import * as PIXI from "pixi.js";

export const DEFAULT_LINE_STYLE_OPTIONS = {
    width: 1,
    color: 0xefefef,
    alpha: 1,
    alignment: 0.5,
    native: false,
    cap: PIXI.LINE_CAP.BUTT,
    join: PIXI.LINE_JOIN.MITER,
    miterLimit: 10,
  };