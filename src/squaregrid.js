import * as PIXI from "pixi.js";
import { PixGrid } from "./pixgrid";

/**
 * Square Grid
 */
export class SquareGrid extends PixGrid {
  /**
   * Construct Square Grid Object
   * @param {number} width width of grid
   * @param {number} height height of grid
   * @param {number} cellSize size in pixels of each cell in grid
   * @param {PIXI.ILineStyleOptions?} lineStyleOptions line width options
   */
  constructor(width, height, cellSize, lineStyleOptions = null) {
    super(width, height, cellSize, lineStyleOptions);
  }

  /**
   * Draw grid shape
   * @returns {SquareGrid} Good for chaining method calls
   */
  drawGrid() {
    // Draw horizontal lines
    for (let i = 0; i <= this.gHeight(); i++) {
      this.moveTo(0, i * this.cell());
      this.lineTo(this.gWidth() * this.cell(), i * this.cell());
    }

    // Draw vertical lines
    for (let i = 0; i <= this.gWidth(); i++) {
      this.moveTo(i * this.cell(), 0);
      this.lineTo(i * this.cell(), this.gHeight() * this.cell());
    }

    return this;
  }
}
