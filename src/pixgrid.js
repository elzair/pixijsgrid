import * as PIXI from "pixi.js";
import { DEFAULT_LINE_STYLE_OPTIONS } from "./constants";

/**
 * Base class for a grid
 */
export class PixGrid extends PIXI.Graphics {
    /**
     * Width and height of each cell in pixels
     * @type {number}
     */
    #cellSize;
  
    /**
     * @returns {number}
     */
    cell() {
      return this.#cellSize;
    }
  
    /**
     * @param {number} cellSize width and height of each cell in pixels
     * @returns {PixGrid} Good for chaining method calls
     */
    cellSize(cellSize) {
      this.#cellSize = cellSize;
      return this;
    }
  
    /**
     * Width of grid
     * @type {number}
     */
    #gridWidth;
  
    gWidth() {
      return this.#gridWidth;
    }
  
    /**
     * @param {number} gridWidth width of grid
     * @returns {PixGrid} Good for chaining method calls
     */
    gridWidth(gridWidth) {
      this.#gridWidth = gridWidth;
      return this;
    }
  
    /**
     * Height of grid
     * @type {number}
     */
    #gridHeight;
  
    /**
     * @returns {number}
     */
    gHeight() {
      return this.#gridHeight;
    }
  
    /**
     * @param {number} gridHeight height of grid
     * @returns {PixGrid} Good for chaining method calls
     */
    gridHeight(gridHeight) {
      this.#gridHeight = gridHeight;
      return this;
    }
  
    /**
     * Construct Grid Object
     * @param {number} width width of grid
     * @param {number} height height of grid
     * @param {number} cellSize size in pixels of each cell in grid
     * @param {PIXI.ILineStyleOptions?} lineStyleOptions line width options
     */
    constructor(width, height, cellSize, lineStyleOptions = null) {
      super();
      this.#gridWidth = width;
      this.#gridHeight = height;
      this.#cellSize = cellSize;
      this.lineStyle({ ...DEFAULT_LINE_STYLE_OPTIONS, ...lineStyleOptions });
    }
  }