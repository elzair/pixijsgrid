import * as PIXI from "pixi.js";
import { PixGrid } from "./pixgrid";
import { defineHex, Grid, Hex, rectangle } from 'honeycomb-grid'

/**
 * Hex Grid
 */
export class HexGrid extends PixGrid {
    /**
     * Type of hexagon to use
     * @type {Hex}
     */
    #hex;
  
    /**
     * @returns {Hex}
     */
    hex() {
      return this.#hex;
    }
  
    /**
     * @param {number} cellSize width and height of each cell in pixels
     * @returns {PixGrid} Good for chaining method calls
     */
    cellSize(cellSize) {
      super.cellSize(cellSize);
      // this.#radius = cellSize / 2.0;
      return this;
    }

    /**
     * @type {Grid}
     */
    #grid

    /**
     * 
     * @returns {Grid}
     */
    grid() {
      return this.#grid;
    }

  
    /**
     * Construct Square Grid Object
     * @param {number} width width of grid
     * @param {number} height height of grid
     * @param {number} cellSize size in pixels of each cell in grid
     * @param {PIXI.ILineStyleOptions?} lineStyleOptions line width options
     */
    constructor(width, height, cellSize, lineStyleOptions = null) {
      super(width, height, cellSize, lineStyleOptions);
      // @ts-ignore
      this.#hex = defineHex({ dimensions: cellSize, origin: 'topLeft'});
      this.#grid = new Grid(Hex, rectangle({ width: this.gWidth(), height: this.gHeight() }));
    }
  
    /**
     * Draw grid shape
     * @returns {HexGrid} Good for chaining method calls
     */
    drawGrid() {
      this.#grid.forEach(hex => this.drawShape(new PIXI.Polygon(this.#hex.corners)));
      return this;
    }
  }
  